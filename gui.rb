require 'gosu'
require 'pry'
require_relative 'lib/player'
require_relative 'lib/colors'
require_relative 'lib/PathFinder'
require_relative 'lib/Range'
require_relative 'lib/DrawWithGosu'
require_relative 'lib/field'

class Main < Gosu::Window
  include Gosu

  def initialize(field, destination)
    super screen_width, screen_height - 100

    @field = field
    @draw = DrawWithGosu.new 60
    @f = Field.new field, @draw
    @player = Player.new(0, 0, 1, @draw, Colors::MAGENTA)
    @bot = Player.new(0, 0, 1, @draw, Colors::PURPLE)
    @objects = [ @player, @bot ]
    start_new_round
  end

  def update
    @objects.each {|o| o.update}
    check_game_status
  end

  def button_up(id)
    case id
    when KB_ESCAPE
      close!
    when KB_SPACE
      start_new_round
      find_route_and_draw randomise_pos @field[0].size, @field.size
    when MS_LEFT
      mouse_pos = @draw.from_screen mouse_x, mouse_y
      find_route_and_draw mouse_pos if position_valid? mouse_pos
    else
      move_player button_id_to_char(id)
      super
    end
  end

  def draw
    @draw.set_window_size width, height, @field.max{|a| a.size}.size, @field.size
    @f.draw
    @objects.each {|o| o.draw}

    unless @knows_where_to_go
      @draw.rect @draw.from_screen(mouse_x, mouse_y), 0.5, Colors::GREEN
    else
      @draw.rect @destination, 0.9, Colors::GREEN
      @draw.rect @destination, 0.7, Colors::GREY_DARK
    end

    @player_moves.each do |move|
      @draw.text move.key, move.pos, Colors::BLUE_LIGHT
    end

    @draw.text @info_text, [10, -1], Colors::BLUE_LIGHT if @info_text
  end

  private

  def check_game_status
    if @destination == @player.cur_pos
      stop_round 'you win!'
    elsif @destination == @bot.cur_pos
      stop_round 'he won :('
    end
  end

  def stop_round(text)
    @cur_move.kill if @cur_move
    @cur_move = nil
    @info_text = text
  end

  def start_new_round
    @info_text = nil
    player_pos = randomise_pos @field[0].size / 2, @field.size / 2
    bot_pos = randomise_pos (@field.size[0] / 2)...@field[0].size, (@field.size / 2)...@field.size

    @player.cur_pos = player_pos
    @bot.cur_pos = bot_pos
    generate_move_keys player_pos
  end

  def randomise_pos(max_x, max_y)
    loop do
      pos = [
        Random.rand(max_x),
        Random.rand(max_y)
      ]

      return pos if position_valid? pos
    end
  end

  def move_player(direction)
    move = @player_moves.find {|m| m.key == direction}
    unless move
      @player.shake
      return
    end

    @player.move_to move.pos
    generate_move_keys move.pos
  end

  KEYS = %w[a s d f g h j k l ;]
  KeyMove = Struct.new :key, :pos
  def generate_move_keys(pos)
    left = [@player.cur_pos[0] - 1, @player.cur_pos[1]]
    right = [@player.cur_pos[0] + 1, @player.cur_pos[1]]
    top = [@player.cur_pos[0], @player.cur_pos[1] - 1]
    bottom = [@player.cur_pos[0], @player.cur_pos[1] + 1]

    new_keys = KEYS.sample 4
    @player_moves = []
    @player_moves << KeyMove.new(new_keys[0], top) if position_valid? top
    @player_moves << KeyMove.new(new_keys[1], bottom) if position_valid? bottom
    @player_moves << KeyMove.new(new_keys[2], left) if position_valid? left
    @player_moves << KeyMove.new(new_keys[3], right) if position_valid? right
  end

  def position_valid?(point)
    point[1] >= 0 and point[1] < @field.size and
      point[0] >= 0 and point[0] < @field[point[1]].size and
      @field[point[1]][point[0]] == 0
  end

  def find_route_and_draw(destination)

    @knows_where_to_go = true
    @destination = destination
    @cur_move.kill if @cur_move
    @cur_move = Thread.new do

      puts "searching path from #{@bot.cur_pos} to #{destination}"
      paths = PathFinder.for(@field)
        .find PathRange.from(@bot.cur_pos).to destination

      paths.first.each do |path|
        puts "steping to #{path}"
        @bot.move_to path
        sleep 1
      end
      @knows_where_to_go = false
    end
    @cur_move.abort_on_exception = true
  end

end

field = [
  [0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0],
  [0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0],
  [0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0],
  [0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0],
  [0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
  [0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0],
  [0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0],
  [1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 1, 0, 0, 0],
  [0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0],
  [0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0],
  [0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0],
  [0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0],
  [0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1],
  [1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0],
  [0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0]
]
Main.new(field,[15, 6]).show
