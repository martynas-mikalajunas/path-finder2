require 'gosu'

class DrawWithGosu
  include Gosu

  def initialize(size)
    @grid_size = size
    @x_offset, @y_offset = 150, 100
    @font = Font.new 30, :name => 'Inconsolata'
  end

  def set_window_size(w,h, user_w, user_h)
    @width, @height = w, h
    @x_offset = (w - to_screen(user_w)) / 2
    @y_offset = (h - to_screen(user_h)) / 2
  end

  def to_screen(point)
    if point.is_a?(Array)
      return [@x_offset + to_screen(point[0]), @y_offset + to_screen(point[1])]
    end
    point * @grid_size
  end

  def from_screen(x, y)
    [((x - @x_offset) / @grid_size).to_i, ((y - @y_offset) / @grid_size).to_i]
  end

  def line(s, e, c)
    screen_s = to_screen s
    screen_e = to_screen e
    color = Color.new c

    draw_line screen_s[0], screen_s[1], color, screen_e[0], screen_e[1], color
  end

  def rect(tl, w, c)
    screen_top_lef = to_screen tl
    width = to_screen w

    delta = (@grid_size - width) / 2
    draw_rect screen_top_lef[0] + delta, screen_top_lef[1] + delta, width, width, Color.new(c)
  end

  def fill_background(c)
    draw_rect 0, 0, @width, @height, Color.new(c)
  end

  def raw_draw_rect(x, y, wx, wy, c)
    draw_rect x,y,wx,wy,Color.new(c)
  end

  def milliseconds
    Gosu.milliseconds
  end

  def text( text, point, c)
    screen_point = to_screen(point)
    dx = dy = @grid_size / 2
    @font.draw_rel text, screen_point[0] + dx, screen_point[1] + dy, 0, 0.5, 0.5, 1, 1, Color.new(c)
  end
end
