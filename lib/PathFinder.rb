require 'pry'
class PathFinder
  def PathFinder.for(field)
    PathFinder.new field
  end

  attr_reader :paths

  def initialize(field)
    @paths = []
    @field = field
  end

  def find(range)
    path = Path.new [range.start]
    candidate_paths = [path]

    while p = candidate_paths.pop do
      #binding.pry
      branches = get_branches p

      branches.each do |branch|
        if branch.last == range.end
          @paths << branch
          candidate_paths.clear
          break
        else
          candidate_paths.unshift branch
        end
      end
      #puts 'candidate routes:'
      #print_routes candidate_paths
    end
    @paths
  end

  def print_routes(paths = nil)
    paths = paths || @paths
    paths.each do |path|
      puts "\t#{path.map{|step|"(#{step[0]},#{step[1]})"}.join('->')}"
    end
  end

  private

  def get_branches(path)
    branches = []
    possible_steps = get_possibilities path.last
    possible_steps.each do |step|
      unless path.include?(step)
        new_path = path.clone
        branches << (new_path << step)
      end
    end
    branches
  end

  DIRECTIONS = [
    # y    x
    [-1, [ 0  ]],
    [ 0, [-1,1]],
    [ 1, [ 0  ]]
  ]

  def get_possibilities(point)
    result = []

    DIRECTIONS.each do |row|
      y = point[1] + row[0]
      row[1].each do |col|
        x = point[0] + col
        result << [x, y] if valid_route?(x, y)
      end
    end
    result
  end

  def valid_route?(x, y)
    index_withwin_bounds?( y, @field) &&
      index_withwin_bounds?( x, @field[y]) &&
      spot_passable?(x,y)
  end

  def index_withwin_bounds?(index, array)
    index >= 0 && index < array.size
  end

  def spot_passable?(x, y)
    @field[y][x] == 0
  end
end

class Path < Array
end
