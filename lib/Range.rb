class PathRange
  def PathRange.from(from)
    PathRange.new from
  end


  attr_reader :start, :end

  def initialize(start)
    @start = start
  end

  def to(e)
    @end = e
    self
  end
end
