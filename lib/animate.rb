require 'pry'

class Animate
  private
  Func = Struct.new(:when, :f)

  def initialize(start_point, start_time)
    @start, @start_time = start_point, start_time
    @start_point = [start_point[0], start_point[1]]
  end

  public
  def self.from(start_point, start_time)
    Animate.new start_point, start_time
  end

  def to(point)
    @end = point
    @direction = @start[0] == @end[0] ? :y : :x
    self
  end

  def shake_in(time_in_ms)
    @direction = :x
    @end = @start_point

    @funcs = [
      Func.new(
        @start_time..@start_time + time_in_ms,
        NormalizedFunc.new(
          ->(t){Math::sin(t)},
          10 * Math::PI,
          2.0,
          10,
          time_in_ms,
          @start[0],
          @start_time))
    ]
    self
  end

  def bounce_in(time_in_ms)
    @end_time = @start_time + time_in_ms
    @change_time = @start_time + 0.8 * time_in_ms

    coord = @direction == :y ? 1 : 0
    s = @start[coord]
    e = @end[coord]

    f1 = NormalizedFunc.new(->(t){t ** 2},
                             200.0,
                             200.0**2,
                             e - s,
                             0.8 * time_in_ms,
                             s,
                             @start_time)

    f2 = NormalizedFunc.new(->(t){Math::sin(t)},
                             Math::PI,
                             1.0,
                             (e - s) * 0.2,
                             0.2 * time_in_ms,
                             f1.call(@change_time),
                             @change_time)

    @funcs = [
      Func.new(@start_time...@change_time-1, f1),
      Func.new(@change_time...@end_time+1, f2)
    ]

    self
  end

  def update(t)

    coord = @direction == :y ? 1 : 0
    result = @end[coord]
    @funcs.each do |f|
      if f.when.include? t
        result = f.f.call(t).to_i
        break
      end
    end

    #binding.pry if t > @funcs.last.when.end
    @start[coord] = result
  end
end

class NormalizedFunc
  def initialize(func, delta_x, delta_y, delta_s, delta_t, start_s, start_t)
    @func = func
    @start_s, @start_t = start_s, start_t

    @ks = delta_s / delta_y
    @kt = delta_x / delta_t
  end

  def call(t)
    #binding.pry
    @start_s + @ks * @func.call(@kt*(t - @start_t))
  end
end
