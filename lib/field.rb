require_relative 'colors'
class Field

  def initialize(field, draw)
    @field, @draw = field, draw
  end

  def draw
    @draw.fill_background Colors::GREY_DARK

    (0..@field[0].size).each do |x|
      @draw.line [x, 0], [x, @field.size], Colors::GRAY_LIGHTEST
    end

    (0..@field.size).each do |y|
      @draw.line [0, y], [@field[0].size, y], Colors::GRAY_LIGHTEST
    end

    @field.each_with_index do |row, y|
      row.each_with_index do |item, x|
        @draw.rect [x,y], 1, Colors::YELLOW_DARK unless item == 0
      end
    end
  end
end
