require_relative 'animate'
require_relative 'colors'
require 'gosu'

class Player
  attr_reader :x, :y

  def initialize (x,y,w, draw, c = Colors::MAGENTA)
    @draw = draw
    @cur_pos = [x,y]
    @color = c
    @screen_coords = @draw.to_screen [x, y]
    @width = @draw.to_screen w
  end

  def cur_pos
    [@cur_pos[0], @cur_pos[1]]
  end

  def cur_pos=(pos)
    @cur_pos = pos
    @screen_coords = @draw.to_screen pos
  end

  def update
    if @current_animation
      @current_animation.update @draw.milliseconds
    else
      @screen_coords = @draw.to_screen @cur_pos
    end
    #print "\rx: #{@x}, y:#{@y}, time: #{time}                                      "
  end

  def move_to(point)
    @screen_coords = @draw.to_screen @cur_pos
    @current_animation = Animate
      .from(@screen_coords, @draw.milliseconds)
      .to(@draw.to_screen(point))
      .bounce_in(600)
    @cur_pos = point
  end

  def shake
    @screen_coords = @draw.to_screen @cur_pos
    @current_animation = Animate
      .from(@screen_coords, @draw.milliseconds)
      .shake_in(300)
  end

  def draw
    @draw.raw_draw_rect @screen_coords[0], @screen_coords[1], @width, @width, @color
  end
end
