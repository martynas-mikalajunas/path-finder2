require_relative '../lib/animate'

describe Animate do
  it 'calculates distance based on time' do
    #a = Animate.new 0, 100, 10
    a = Animate.from( [10,5], 10).to([110, 5]).bounce_in(20)

    expected_results = [
      a.update(10),
      a.update(15),
      a.update(26),
      a.update(26.1),
      a.update(28),
      a.update(29.5),
      a.update(30)
    ]
    expect(expected_results).to eq [10, 19, 110, 111, 130, 117, 110]
  end
end
