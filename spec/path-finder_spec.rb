require_relative '../lib/PathFinder'
require_relative '../lib/Range'

describe 'pathfinder' do
  it 'finds path between 2 points' do
    field = [
#      0  1  2   3  4
      [0, 1, 0, -1, 0], # 0
      [0, 1, 0,  0, 0], # 1
      [0, 0, 0,  0, 1], # 2
    ]

    expected_path = [[0,0], [0,1], [0,2], [1,2], [2,2],[3,2],[3,1],[4,1],[4,0]]

    pf = PathFinder.for(field)
    paths = pf.find PathRange.from([0,0]).to([4,0])

    puts 'found routes:'
    pf.print_routes
    expect(paths).to include expected_path
  end

  it 'minimal case' do
    field = [
      [0,0,0]
    ]

    expected_path = [[0,0], [1,0], [2,0]]
    paths = PathFinder.for(field).find PathRange.from([0,0]).to([2,0])
    expect(paths[0]).to eq expected_path
  end

  it 'can find multiple paths' do
    field = [
      [0,0,0],
      [0,0,0],
      [0,0,0]
    ]

    expected_path = [[0,0], [0,1], [0,2],[1,2],[2,2]]
    paths = PathFinder.for(field).find PathRange.from([0,0]).to([2,2])
    expect(paths).to include expected_path
  end
end
